from random import randint, shuffle # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import requests

MIN_COMPLEX = 1
MAX_COMPLEX = 4

LOWER = list("abcdefghijklmnopqrstuvwxyz")
UPPER = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
DIGIT = list("0123456789")
SPECIAL_CHAR = list("!@#$%^&*()?")
DB_PATH = './users.db'
USERS_TABLE = 'users'

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    if length < MAX_COMPLEX:
        raise Exception('Password too short')

    if complexity > MAX_COMPLEX or complexity < MIN_COMPLEX:
        raise Exception('Invalid complexity range')

    password = ''
    char_set = []

    # to store at least one random characters of lower complexity levels
    # for example: to avoid a complexity 4 password without any digit
    required_char = []

    # create a character set based on complexity
    if complexity >= 1:
        char_set.extend(LOWER)
        required_char.append(random_char(LOWER))
    if complexity >= 2:
        char_set.extend(DIGIT)
        required_char.append(random_char(DIGIT))
    if complexity >= 3:
        char_set.extend(UPPER)
        required_char.append(random_char(UPPER))
    if complexity == 4:
        char_set.extend(SPECIAL_CHAR)
        required_char.append(random_char(SPECIAL_CHAR))

    # shuffling so required_char does not appear in order of low complexity to high complexity
    shuffle(required_char)

    # add required character to password first
    password += ''.join(required_char)

    for _ in range(length - len(password)):
        # select a random character from char_set
        password += random_char(char_set)

    return password

def random_char(char_set: list) -> str:
    """Return a random character from a list of characters

    :param char_set: set of character to choose from
    """
    return char_set[randint(0, len(char_set) - 1)]

def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    complex_level = 0
    length = len(password)

    # flags for checking off character types
    lower = False
    digit = False
    upper = False
    special_char = False

    # check for character types in the password
    for char in password:
        if char in LOWER:
            lower = True
        if char in DIGIT:
            digit = True
        if char in UPPER:
            upper = True
        if char in SPECIAL_CHAR:
            special_char = True

    # determine complexity level
    if lower and digit and upper and special_char:
        complex_level = 4
    elif lower and digit and upper:
        complex_level = 3
    elif lower and digit:
        complex_level = 2
    else:
        complex_level = 1

    # Complexity level exceptions
    if (complex_level == 1 or complex_level == 2) and length >= 8:
        complex_level += 1

    return complex_level

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """

    insert_query = 'INSERT INTO {0}(name, email) VALUES(?, ?);'.format(USERS_TABLE)

    # get a random user
    try:
        api_respond = requests.get("https://randomuser.me/api/").json()
    except:
        raise Exception('Failed to get an API respond.')

    user = api_respond['results'][0]
    name = '{0} {1}'.format(user['name']['first'], user['name']['last'])
    email = user['email']

    user_data = (name, email)

    try:
        conn = sqlite3.connect(db_path)
        try:
            cur = conn.cursor()
            cur.execute(insert_query, user_data)
            conn.commit()
        except sqlite3.Error:
            print('Unable to add user to database.')
    except sqlite3.Error:
        print('Unable to connect to database.')
    finally:
        conn.close()

if __name__ == '__main__':
    for _ in range(10):
        create_user(DB_PATH)
