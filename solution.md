# Solution Explanation

## `password_gen.py` Module

### `generate_password` Function

I use constants variables for these values because they rarely change. And when changes are needed, they can be one in one place.

```python3
MIN_COMPLEX = 1
MAX_COMPLEX = 4

LOWER = list("abcdefghijklmnopqrstuvwxyz")
UPPER = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
DIGIT = list("0123456789")
SPECIAL_CHAR = list("!@#$%^&*()?")
DB_PATH = './users.db'
USERS_TABLE = 'users'
```

For the `generate_password` function, my approach is to build a set of characters to choose from based on the complexity level. But this brings a problem. Due to randomness, there is a chance that characters of lower complexity are not included. For example, a password complexity of 4 might not have any digit, because characters are picked randomly from the set.

So I have to find a way to include characters of lower complexity. This is what the `required_char` list is for.

```python3
  required_char = []
```

As I add character types into the character set, I also add one random character from each type. That way, a password of higher complexity will have at least one character of each lower level of complexity.

```python3
    if complexity >= 1:
        char_set.extend(LOWER)
        required_char.append(random_char(LOWER))
    if complexity >= 2:
        char_set.extend(DIGIT)
        required_char.append(random_char(DIGIT))
    if complexity >= 3:
        char_set.extend(UPPER)
        required_char.append(random_char(UPPER))
    if complexity == 4:
        char_set.extend(SPECIAL_CHAR)
        required_char.append(random_char(SPECIAL_CHAR))
```

Because the block above add lower complexity characters to `required_char` first, I should shuffle this list to retain the randomness of the password. In other words, so a complexity 4 password does not always start with a lowercase, a digit, an uppercase and a punctuation.

```python3
    shuffle(required_char)
```

From there, I add the shuffled required characters to the final password, followed by random characters from the set.

```python3
    password += ''.join(required_char)

    for _ in range(length - len(password)):
        # select a random character from char_set
        password += random_char(char_set)

    return password
```

### `check_password_level` Function

My approach is to parse the password, find the types of characters it has, then determine the complexity level.

Boolean flags are used to check off character types

```python3
    for char in password:
        if char in LOWER:
            lower = True
        if char in DIGIT:
            digit = True
        if char in UPPER:
            upper = True
        if char in SPECIAL_CHAR:
            special_char = True
```

Then complexity is determined based on characters the password has.

```python3
    if lower and digit and upper and special_char:
        complex_level = 4
    elif lower and digit and upper:
        complex_level = 3
    elif lower and digit:
        complex_level = 2
    else:
        complex_level = 1
```

Adjust complexity level for exceptions:

```python3
    if (complex_level == 1 or complex_level == 2) and length >= 8:
        complex_level += 1
```

### `create_user` Function

I think this function is quite straightforward. Send a get request to the given API. Get data back. Login the database. Run the add query. Then logout.

For network-related stuff, or statements that might throw errors, I use `try` block.

### `main` Function

Add ten random users to database

```python3
if __name__ == '__main__':
    for _ in range(10):
        create_user(DB_PATH)
```

## `test_password.py` Module

This file's main purpose is to test if the `generate_password` and the `check_password_level` functions work properly.

My approach is to generate a lot of random passwords, with different random complexities, then verify each password's complexity against the randomly generated complexity.

The `assert` statement is used. If the ramdom complexity value does not match complexity determined by `check_password_level`, there might be a bug. The code will log 'Fail' to the console. I also print some output for debugging.

```python3
        try:
            assert complex_level == check_password_level(password)
        except AssertionError:
            print(complex_level, password, check_password_level(password))
            print('Fail')
            exit()
```

If after a thousand passwords and no error. The code print 'Success' to the console.

## `generate_password_for_users` Module

Nothing much for this script. Other than the fact that I added more than 10 users to the database, and some of them already have passwords. So I specify in the `fetch_query` that users being collected do not have passwords:

```python3
    fetch_query = 'SELECT * FROM {0} WHERE password IS NULL LIMIT {1};'.format(USERS_TABLE, num_user)
```
