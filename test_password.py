from sys import exit
from password_gen import generate_password, check_password_level, MIN_COMPLEX, MAX_COMPLEX
from random import randint

if __name__ == '__main__':
    for _ in range(1000):
        complex_level = randint(MIN_COMPLEX, MAX_COMPLEX)
        length = randint(5, 25)
        password = generate_password(length, complex_level)
        # Complexity level exceptions
        if (complex_level == 1 or complex_level == 2) and length >= 8:
            complex_level += 1
        try:
            assert complex_level == check_password_level(password)
        except AssertionError:
            print(complex_level, password, check_password_level(password))
            print('Fail')
            exit()
    print('Success')
