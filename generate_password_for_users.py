import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
from password_gen import generate_password, USERS_TABLE, DB_PATH, MIN_COMPLEX, MAX_COMPLEX
from random import randint

if __name__ == '__main__':
    num_user = 10
    low_len_limit = 6
    high_len_limit = 12
    fetch_query = 'SELECT * FROM {0} WHERE password IS NULL LIMIT {1};'.format(USERS_TABLE, num_user)
    update_query = '''
    UPDATE {0}
    SET
        password = ?
    WHERE
        name = ? AND email = ?;
    '''.format(USERS_TABLE)

    try:
        conn = sqlite3.connect(DB_PATH)
        cur = conn.cursor()
    except sqlite3.Error:
        raise Exception('Unable to connect')

    # get 10 users
    cur.execute(fetch_query)
    users = cur.fetchall()

    for user in users:
        # build query data
        length = randint(low_len_limit, high_len_limit)
        complexity = randint(MIN_COMPLEX, MAX_COMPLEX)
        password = generate_password(length, complexity)
        query_data = (password, user[0], user[2])

        # submit query
        try:
            cur.execute(update_query, query_data)
        except sqlite3.Error:
            raise Exception('Failed to update a query.')

    conn.commit()
    conn.close()
